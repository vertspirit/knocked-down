package main

import (
    "flag"
)

type args struct {
    host      string
    port      int
    config    string
    subpath   string
    syslog    bool
    graceful  bool
    debug     bool
}

func parseFlags() *args {
    arg := &args{}
    flag.StringVar(&arg.config, "config", "config/config.yaml", "The configuration file for aution server.")
    flag.StringVar(&arg.config, "c", "config.yaml", "The configuration file for aution server. (shorten)")
    flag.StringVar(&arg.host, "host", "0.0.0.0", "The ip address of hostname of this server.")
    flag.StringVar(&arg.host, "h", "0.0.0.0", "The ip address or hostname of this server. (shorten)")
    flag.IntVar(&arg.port, "port", 1999, "The port of this server.")
    flag.IntVar(&arg.port, "p", 1999, "The port of this server. (shorten)")
    flag.StringVar(&arg.subpath, "subpath", "/", "The subpath for uri root of this server.")
    flag.StringVar(&arg.subpath, "b", "/", "The subpath for uri root of this server. (shorten)")
    flag.BoolVar(&arg.syslog, "syslog", false, "Enable the logging by syslog to local or remote.")
    flag.BoolVar(&arg.syslog, "s", false, "Enable the logging by syslog to local or remote. (shorten)")
    flag.BoolVar(&arg.graceful, "graceful", false, "Use graceful mode to start up this server.")
    flag.BoolVar(&arg.graceful, "g", false, "Use graceful mode to start up this server. (shorten)")
    flag.BoolVar(&arg.debug, "debug", false, "Run in debug mode to analyze performace by pprof.")
    flag.BoolVar(&arg.debug, "d", false, "Run in debug mode to analyze performace by pprof. (shorten)")
    flag.Parse()
    return arg
}
