#!/bin/sh

STARTDIR=$(dirname "$(readlink -f $0)")
cd "${STARTDIR}/.."

read -p 'Enter the version [1.0.0]: ' VERSION
VERSION=${VERSION:-1.0.0}
BUILD_DATE=$(date +%F)

go build -o test/knocked-down -mod mod -tags doc \
  -ldflags "-X main.Version=${VERSION} -X main.Build=${BUILD_DATE}"
