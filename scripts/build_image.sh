#!/bin/sh

STARTDIR=$(dirname "$(readlink -f $0)")
cd "${STARTDIR}/.."

read -p 'Enter the image name [knocked-down]: ' IMAGE_NAME
IMAGE_NAME="${IMAGE_NAME:-knocked-down}"

read -p 'Enter the version [1.0.0]: ' APP_VERSION
APP_VERSION="${APP_VERSION:-1.0.0}"

read -p 'Enter the image tag [latest]: ' TAG
TAG="${TAG:-latest}"

read -p 'Enter the timezone [Asia/Taipei]: ' TZ
TZ="${TZ:-Asia/Taipei}"

export TZ=${TZ}
export APP_VERSION=${APP_VERSION}
export BUILD_DOC="-tags doc"

docker build \
  --build-arg TZ \
  --build-arg APP_VERSION \
  --build-arg BUILD_DOC \
  --tag "${IMAGE_NAME}:${TAG}" \
  --file ./build/Dockerfile-server \
  .