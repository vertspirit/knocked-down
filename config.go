package main

import (
    "log"
    "io/ioutil"
    "gopkg.in/yaml.v3"
    "gopkg.in/natefinch/lumberjack.v2"
    "knocked-down/pkg/aution"
)

type serverConfig struct {
    Server  *aution.AutionServer  `yaml:"server"`
    Syslog  *syslogSettings       `yaml:"syslog"`
    Log     *lumberjack.Logger    `yaml:"log"`
}

type syslogSettings struct {
    host      string  `yaml:"host"`
    port      int     `yaml:"port"`
    protocol  string  `yaml:"protocol"`
}

func parseConfig(filename string) *serverConfig {
    var cfg serverConfig
    data, err := ioutil.ReadFile(filename)
    if err != nil {
        log.Fatal(err)
    }
    if err = yaml.Unmarshal(data, &cfg); err != nil {
        log.Fatal(err)
    }
    return &cfg
}

