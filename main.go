package main

import (
    "fmt"
)

var (
    Version  string
    Build    string
)

func main() {
    settings := parseFlags()
    conf := parseConfig(settings.config)
    fmt.Printf("Verion: %15s\n", Version)
    fmt.Printf("Build Date: %15s\n", Build)
    addr := fmt.Sprintf("%s:%d", settings.host, settings.port)
    conf.Server.Version = Version
    conf.Server.StartHub()
    conf.Server.StartServer(addr, settings.graceful, conf.Server.TLS)
}
