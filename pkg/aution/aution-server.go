package aution

import (
    "log"
    "net/http"
    "time"
    "github.com/gorilla/websocket"
)

type AutionServer struct {
    WsConfig        *websocketSettings  `yaml:"websocket"`
    WsUpgrader      websocket.Upgrader
    AutionHub       *autionHub
    Version         string
    Subpath         string
    ServiceName     string              `yaml:"name"`
    RealIpHeader    string              `yaml:"reai-ip-header"`
    Cert            string              `yaml:"cert"`
    PrivKey         string              `yaml:"privkey"`
    TLS             bool                `yaml:"tls"`
    EnablePprof     bool                `yaml:"enable_pprof"`
    ReadTimeout     int                 `yaml:"read_timeout"`
    WriteTimeout    int                 `yaml:"write_timeout"`
    MaxHeaderBytes  int                 `yaml:"max_header_bytes"`
}

type websocketSettings struct {
    Subprotocols      []string  `yaml:"subprotocols"`
    ReadBufferSize    int       `yaml:"read_buffer_size"`
    WriteBufferSize   int       `yaml:"write_buffer_size"`
    HandshakeTimeout  int       `yaml:"handshake_timeout"`
    PingInterval      int64     `yaml:"ping_interval"`
    PongWaitTime      int64     `yaml:"pong_wait_time"`
    WriteWaitTime     int64     `yaml:"write_wait_time"`
}

type wsConnection struct {
    ws   *websocket.Conn
    bid  chan string               // send bid message for aution
}

type autionSubscription struct {
    conn            *wsConnection  `json:"-"`
    saleNumber      string         `json:"sale-number"`
    auctioneer      string         `json:"auctioneer"`
    invitationCode  string         `json:"invitation-code"`
    maxSize         int64          `json:"max-size"`
    reservePrice    int            `json:"reserve-price"`
    minEstimate     int            `json:"min-estimate"`
    maxEstimate     int            `json:"max-estimate"`
    bidIncrement    int            `json:"bid-increment"`    // 10% reservePrice in most case
    hammerPrice     int            `json:"-"`
    boughtIn        bool           `json:"-"`                // pass or unsold
}

type autionBid struct {
    saleNumber  string
    message     string
    paddle      int
}

type autionHub struct {
    autionPool  map[string]map[*wsConnection]bool
    register    chan *autionSubscription
    unregister  chan *autionSubscription
    broadcast   chan *autionBid
}

func (ah *autionHub) listenEvents() {
    for {
        select {
        case reg := <-ah.register:
            conns := ah.autionPool[reg.saleNumber]
            if conns == nil {
                conns = make(map[*wsConnection]bool)
                ah.autionPool[reg.saleNumber] = conns
            }
            ah.autionPool[reg.saleNumber][reg.conn] = true
        case unreg := <-ah.unregister:
            conns := ah.autionPool[unreg.saleNumber]
            if conns != nil {
                _, ok := conns[unreg.conn]
                if ok {
                    delete(conns, unreg.conn)
                    close(unreg.conn.bid)
                    if len(conns) == 0 {
                        delete(ah.autionPool, unreg.saleNumber)
                    }
                }
            }
        case bid := <-ah.broadcast:
            conns := ah.autionPool[bid.saleNumber]
            for c := range conns {
                select {
                case c.bid <-bid.message:
                default:
                    close(c.bid)
                    delete(conns, c)
                    if len(conns) == 0 {
                        delete(ah.autionPool, bid.saleNumber)
                    }
                }
            }
        }
    }
}

func (as *AutionServer) SetupWebsocket() {
    if as.WsConfig.Subprotocols == nil || len(as.WsConfig.Subprotocols) == 0 {
        as.WsConfig.Subprotocols = []string{ "Sec-WebSocket-Protocol", "Set-Cookie" }
    }
    if as.WsConfig.HandshakeTimeout == 0 {
        as.WsConfig.HandshakeTimeout = 15
    }
    as.WsUpgrader = websocket.Upgrader {
        ReadBufferSize:    as.WsConfig.ReadBufferSize,
        WriteBufferSize:   as.WsConfig.WriteBufferSize,
        Subprotocols:      as.WsConfig.Subprotocols,
        HandshakeTimeout:  time.Duration(as.WsConfig.HandshakeTimeout) * time.Second,
    }
}

func (as *AutionServer) wsAutionHandler(w http.ResponseWriter, r *http.Request, atsub *autionSubscription) {

    as.WsUpgrader.CheckOrigin = func(r *http.Request) bool {
        return true
    }
    conn, err := as.WsUpgrader.Upgrade(w, r, nil)
    if err != nil {
        log.Printf("Fail to upgrade: %v \n", err)
        return
    }
    // challengeKey := r.Header.Get("Sec-Websocket-Key")
    wsConn := &wsConnection {
        ws:  conn,
        bid: make(chan string, 256),
    }
    atsub.conn = wsConn
    as.AutionHub.register <- atsub

    go atsub.keepWriting(
        time.Duration(as.WsConfig.PingInterval) * time.Second,
        time.Duration(as.WsConfig.WriteWaitTime) * time.Second,
    )
    go atsub.keepReading(
        time.Duration(as.WsConfig.PongWaitTime) * time.Second,
        as.AutionHub,
    )
}

func (ass *autionSubscription) keepWriting(pingInterval, writeWait time.Duration) {
    conn := ass.conn
    ticker := time.NewTicker(pingInterval)
    defer func() {
        ticker.Stop()
        conn.ws.Close()
    }()

    for {
        select {
        case <-ticker.C:
            err := conn.ws.WriteMessage(websocket.PingMessage, []byte{})
            if err != nil {
                return
            }
        case msg, ok := <-conn.bid:
            if !ok {
                conn.ws.WriteMessage(websocket.CloseMessage, []byte{})
                return
            }
            conn.ws.SetWriteDeadline(time.Now().Add(writeWait))
            err := conn.ws.WriteMessage(websocket.TextMessage, []byte(msg))
            if err != nil {
                log.Printf("Fail to send message for bid: %v \n", err)
                return
            }
        }
    }
}

func (ass *autionSubscription) keepReading(pongWaitTime time.Duration, ah *autionHub) {
    wsConn := ass.conn.ws
    defer func() {
        ah.unregister <-ass
        wsConn.Close()
    }()

    wsConn.SetReadLimit(ass.maxSize)

    wsConn.SetReadDeadline(time.Now().Add(pongWaitTime))
    wsConn.SetPongHandler(func(string) error {
        wsConn.SetReadDeadline(time.Now().Add(pongWaitTime))
        return nil
    })

    for {
        _, msg, err := wsConn.ReadMessage()
        if err != nil {
            if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
                log.Printf("Connection closed: %v \n", err)
            }
            break
        }
        bid := &autionBid {
            message:     string(msg),
            saleNumber:  ass.saleNumber,
        }
        ah.broadcast <-bid
    }
}
