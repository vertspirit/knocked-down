package aution

import (
    "net/http"
    "os"
    "os/signal"
    "syscall"
    "time"
    "log"
    "context"
    "github.com/gin-gonic/gin"
    "github.com/gin-contrib/pprof"
    "github.com/fvbock/endless"
)

func (as *AutionServer) StartHub() {
    // start up autionhub events listener before gin server
    as.AutionHub = &autionHub {
        autionPool:  make(map[string]map[*wsConnection]bool),
        register:    make(chan *autionSubscription),
        unregister:  make(chan *autionSubscription),
        broadcast:   make(chan *autionBid),
    }
    go as.AutionHub.listenEvents()
}

func (as *AutionServer) SetupRouter() *gin.Engine {
    router := gin.New()
    router.Use(gin.Logger())
    if as.EnablePprof {
        pprof.Register(router, as.Subpath + "/debug/pprof")
    }
    router.SetTrustedProxies(nil)
    router.RemoteIPHeaders = []string {
        "X-Forwarded-For",
        "X-Real-IP",
    }
    if as.RealIpHeader == "" {
        as.RealIpHeader = "X-Forwarded-For"
    }
    router.GET(as.Subpath + "/healthcheck", as.healthcheckHandler)
    router.GET(as.Subpath + "/ws/:salenumber", as.autionHandler)
    return router
}

func (as *AutionServer) StartServer(addr string, graceful bool, ssl bool) {
    router := as.SetupRouter()
    if graceful {
        endless.DefaultReadTimeOut = time.Duration(as.ReadTimeout) * time.Second
        endless.DefaultWriteTimeOut = time.Duration(as.WriteTimeout) * time.Second
        endless.DefaultMaxHeaderBytes = as.MaxHeaderBytes << 20 // default is max 2 MB
        srv := endless.NewServer(addr, router)
        srv.BeforeBegin = func(add string){
            log.Printf("Actual PID: %d\n", syscall.Getpid())
        }
        if ssl {
            if err := srv.ListenAndServeTLS(as.Cert, as.PrivKey); err != nil {
                log.Printf("TLS Server Error: %v\n", err)
            }
        } else {
            if err := srv.ListenAndServe(); err != nil {
                log.Printf("Server Error: %v\n", err)
            }
        }
    } else {
        srv := &http.Server {
            Addr:           addr,
            Handler:        router,
            ReadTimeout:    time.Duration(as.ReadTimeout) * time.Second,
            WriteTimeout:   time.Duration(as.WriteTimeout) * time.Second,
            MaxHeaderBytes: as.MaxHeaderBytes << 20,
        }
        go func() {
            if ssl {
                if err := srv.ListenAndServeTLS(as.Cert, as.PrivKey); err != nil {
                    log.Printf("Server Listen Error: %s\n", err)
                }
            } else {
                if err := srv.ListenAndServe(); err != nil {
                    log.Printf("Server Listen Error: %s\n", err)
                }
            }
        }()
        quit := make(chan os.Signal, 1)
        signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
        <-quit
        log.Println("Shutting down server...")
        ctx, cancel := context.WithTimeout(context.Background(), 5 * time.Second)
        defer cancel()
        if err := srv.Shutdown(ctx); err != nil {
            log.Fatal("Server forced to shutdown:", err)
        }
    }
}

func (as *AutionServer) healthcheckHandler(c *gin.Context) {
    c.JSON(http.StatusOK, gin.H {
        "version": as.Version,
        "status":  "online",
    })
}

func (as *AutionServer) autionHandler(c *gin.Context) {
    saleNumber := c.Param("salenumber")
    code := c.Request.Header["Invitation-Code"][0]
    if code == "" {
        c.Abort()
    }
    autionSub := &autionSubscription {
        saleNumber:     saleNumber,
        invitationCode: code,
        reservePrice:   1000,
        maxSize:        1024,
    }
    as.wsAutionHandler(c.Writer, c.Request, autionSub)
}
